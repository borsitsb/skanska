<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $to = $_POST['to'];
    $subject = 'Karácsonyi üdvözlet';

    $headers = "From: info@skanskagreetings.com" . "\r\n";
    $headers .= "Reply-To: info@skanskagreetings.com" . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    $message = "<!doctype html>
<html>
    <head>
    <meta charset=\"utf-8\">
    <!-- utf-8 works for most cases -->
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <title>EmailTemplate-Responsive</title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

    <!-- CSS Reset -->
    <style type=\"text/css\">
/* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
html,  body {
    margin: 0 !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
}
/* What it does: Stops email clients resizing small text. */
* {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}
/* What it does: Forces Outlook.com to display emails full width. */
.ExternalClass {
    width: 100%;
}
/* What is does: Centers email on Android 4.4 */
div[style*=\"margin: 16px 0\"] {
    margin: 0 !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table,  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
}
/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
}
table table table {
    table-layout: auto;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
    -ms-interpolation-mode: bicubic;
}
/* What it does: Overrides styles added when Yahoo's auto-senses a link. */
.yshortcuts a {
    border-bottom: none !important;
}
/* What it does: Another work-around for iOS meddling in triggered links. */
a[x-apple-data-detectors] {
    color: inherit !important;
}
</style>

    <!-- Progressive Enhancements -->
    <style type=\"text/css\">

        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>
    </head>
    <body bgcolor=\"#e0e0e0\" width=\"100%\" style=\"margin: 0;\" yahoo=\"yahoo\">
    <table bgcolor=\"#e0e0e0\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;\">
      <tr>
        <td><center style=\"width: 100%;\">
            
            <!-- Visually Hidden Preheader Text : BEGIN -->
            <div style=\"display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;\"> (Optional) This text will appear in the inbox preview, but not the email body. </div>
            <!-- Visually Hidden Preheader Text : END --> 
            
            <!-- Email Header : BEGIN -->
            <table align=\"center\" width=\"600\" class=\"email-container\">
            <tr>
                <td style=\"padding: 20px 0; text-align: center\"><img src=\"http://freevectorlogo.net/wp-content/uploads/2012/10/skanska-vector-logo-400x400.png\" width=\"200\" height=\"200\" alt=\"Skanska\" border=\"0\"></td>
              </tr>
          </table>
            <!-- Email Header : END --> 
            
            <!-- Email Body : BEGIN -->
            <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" bgcolor=\"#ffffff\" width=\"600\" class=\"email-container\">
            
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
                <td class=\"full-width-image\"><img src=\"http://www.norbertbarna.com/skanskaweb/forras/puzzle.jpg\" width=\"600\" alt=\"alt_text\" border=\"0\" align=\"center\" style=\"width: 100%; max-width: 600px; height: auto;\"></td>
              </tr>
            <!-- Hero Image, Flush : END --> 
            
            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td style=\"padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;\"><br> <p style=\"padding: 20px; text-align: center; font-family: sans-serir; font-size: 16px; font-weight: 700; mso-height-rule: exactly; line-height: 20px; color: black;\">Kedves " . $_POST['name'] . "! Karácsonyi üdvözletet küldtek Neked az alábbi szöveggel:</p>
                    " . $_POST['message'] . "
<br>
<!-- Button : Begin -->
                
                <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" style=\"margin: auto\">
                    <tr>
                    <td style=\"border-radius: 3px; background: #222222; text-align: center;\" class=\"button-td\"><a href=\"http://www.google.com\" style=\"background: #f30; border: 15px solid #f30; padding: 0 10px;color: #ffffff; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;\" class=\"button-a\"> 
                      <!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]-->A Button<!--[if mso]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]--> 
                      </a></td>
                  </tr>
                  </table>
                
                <!-- Button : END --></td>
              </tr>
            <!-- 1 Column Text : BEGIN --> 
            
            <!-- Background Image with Text : BEGIN -->
            <tr>
                <td background=\"images/Image_600x230.png\" bgcolor=\"#222222\" valign=\"middle\" style=\"text-align: center; background-position: center center !important; background-size: cover !important;\"><!--[if gte mso 9]>
                    <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"width:600px;height:175px; background-position: center center !important;\">
                    <v:fill type=\"tile\" src=\"assets/Responsive/Image_600x230.png\" color=\"#222222\" />
                    <v:textbox inset=\"0,0,0,0\">
                    <![endif]-->
                
                <div>

                  </div>
                
                <!--[if gte mso 9]>
                    </v:textbox>
                    </v:rect>
                    <![endif]--></td>
              </tr>
            <!-- Background Image with Text : END --> 
            
            <!-- Two Even Columns : BEGIN -->
            <tr>
                <td align=\"center\" valign=\"top\" style=\"padding: 10px;\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
                    <tr>

                    <td class=\"stack-column-center\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
                        <tr>
                        <td style=\"padding: 10px; text-align: center\">&nbsp;</td>
                      </tr>
                        <tr>
                        <td style=\"font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 0 10px 10px; text-align: left;\" class=\"center-on-narrow\"> Sikerekben gazdag, békés, boldog új évet kívánunk </td>
                      </tr>
                      </table></td>
                  </tr>
                  </table></td>
              </tr>
            <!-- Two Even Columns : END --> 
            
            <!-- Three Even Columns : BEGIN -->
            
            <!-- Three Even Columns : END --> 
            
            <!-- Thumbnail Left, Text Right : BEGIN -->
            <tr>
               
              </tr>
            <!-- Thumbnail Left, Text Right : END --> 
            
            <!-- Thumbnail Right, Text Left : BEGIN -->
            <tr>
                <td dir=\"rtl\" align=\"center\" valign=\"top\" width=\"100%\" style=\"padding: 10px;\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                    <tr>
                    <td width=\"33.33%\" class=\"stack-column-center\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                        <tr>

                      </tr>
                      </table></td>
                    
                  </tr>
                  </table></td>
              </tr>
            <!-- Thumbnail Right, Text Left : END -->
            
          </table>
            <!-- Email Body : END --> 
            
            <!-- Email Footer : BEGIN -->
            <table align=\"center\" width=\"600\" class=\"email-container\">
            <tr>
                <td style=\"padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;\"><webversion style=\"color:#cccccc; text-decoration:underline; font-weight: bold;\">Weboldal megtekintése</webversion>
                <br>
                <br>
                2016<br>
                <br>
               </td>
              </tr>
          </table>
            <!-- Email Footer : END -->
            
          </center></td>
      </tr>
    </table>
</body>
</html>
";

    mail($to, $subject, $message, $headers);
}
?>

  <!DOCTYPE html>
  <!--  This site was created in Webflow. http://www.webflow.com -->
  <!--  Last Published: Tue Dec 06 2016 21:41:52 GMT+0000 (UTC)  -->
  <html data-wf-page="584401a6a5d6e37c09a0f92e" data-wf-site="584401a6a5d6e37c09a0f92d">

  <head>
    <meta charset="utf-8">
    <title>ska</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/webflow.css" rel="stylesheet" type="text/css">
    <link href="css/ska.webflow.css" rel="stylesheet" type="text/css">
    <link href="css/snow.css" rel="stylesheet" type="text/css">
    <link href="css/form.css" rel="stylesheet" type="text/css">
    <script src="js/modernizr.js" type="text/javascript"></script>
    <script src="js/popup.js" type="text/javascript"></script>
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
    <style>
    .container-main-header,
      .containter-main-content {
        width: 100%;
        max-width: 1200px;
      }
      
      .container-full {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin-right: auto;
        margin-left: auto;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
      }
      
      .container-main-content {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 100%;
        height: 60px;
        max-width: 1200px;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -ms-flex-item-align: center;
        align-self: center;
        -webkit-box-flex: 0;
        -ms-flex: 0 auto;
        flex: 0 auto;
        background-color: transparent;
      }
      
      .logo {
        position: relative;
        z-index: 33;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 200px;
        margin-left: 15px;
        padding-bottom: 7px;
        padding-left: 0px;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
      }
      
      .flags {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin-right: 100px;
        padding-bottom: 8px;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -webkit-box-ordinal-group: 2;
        -ms-flex-order: 1;
        order: 1;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
      }
      
      .first-section {
        position: static;
        height: 100vh;
        background-color: #f30;
      }
      
      .top-cloud {
        position: relative;
        left: 0px;
        top: 0px;
        right: 0px;
        bottom: 0px;
        display: block;
        overflow: hidden;
        height: 110px;
      }
      
      .top-image {
        position: absolute;
        left: 0px;
        top: -65px;
        z-index: 1;
        display: block;
        overflow: hidden;
        width: 100%;
        max-width: 100%;
      }
      
      .main-content {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 100%;
        max-width: 1180px;
        margin-right: auto;
        margin-left: auto;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
      }
      
      .leftside-img {
        position: relative;
        left: -73px;
        top: -107px;
        right: 0px;
        bottom: 0px;
        z-index: 10;
        overflow: hidden;
        max-width: 18%;
        float: left;
        cursor: pointer;
      }
      
      .second-section {
        position: static;
        height: 60vh;
        background-color: #f30;
      }
      
      .rightside-img {
        position: relative;
        top: -123px;
        right: 21px;
        max-width: 25%;
        float: right;
        -ms-flex-item-align: start;
        align-self: flex-start;
        text-align: right;
      }
      
      .middle-img {
        position: relative;
        top: -79px;
        z-index: 146;
        display: block;
        width: 450px;
        margin-right: auto;
        margin-left: auto;
        padding-right: 0px;
        float: left;
        clear: none;
        -webkit-box-align: start;
        -ms-flex-align: start;
        -ms-grid-row-align: flex-start;
        align-items: flex-start;
        -ms-flex-item-align: start;
        align-self: flex-start;
        background-color: transparent;
      }
      
      .middle-text {
        position: relative;
        z-index: 183;
        display: block;
        height: auto;
        margin-top: 50px;
        padding-top: 0px;
        padding-bottom: 0px;
        float: left;
        clear: left;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-item-align: end;
        align-self: flex-end;
        -webkit-box-flex: 0;
        -ms-flex: 0 auto;
        flex: 0 auto;
        background-color: #f30;
        font-family: 'Skanskasanspro regular', sans-serif;
        text-align: center;
      }
      
      .text-block {
        display: block;
        float: left;
        clear: left;
        background-color: #f30;
        font-family: 'Skanskasanspro regular', sans-serif;
        color: #fff;
        font-size: 14px;
        line-height: 21px;
        font-weight: 400;
        text-align: center;
        letter-spacing: 1px;
        text-transform: capitalize;
      }
      
      .middle-text-heading {
        margin-bottom: 30px;
        font-family: 'Skanskasanspro bold', sans-serif;
        color: #fff;
        font-size: 28px;
        line-height: 32px;
      }
      
      ._2nd-section-img-container {
        position: static;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 35%;
        margin-right: auto;
        margin-left: auto;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
      }
      
      ._2nd-container-left-imgdiv {
        position: relative;
        left: -45px;
        top: -228px;
        right: 0px;
        z-index: 2;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 20%;
        height: 100%;
        margin-right: 0px;
        padding-left: 0px;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-item-align: start;
        align-self: flex-start;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
      }
      
      ._2nd-container-right-imgdiv {
        position: relative;
        left: 135px;
        top: -250px;
        right: 0px;
        bottom: 0px;
      }
      
      .footer {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        height: 100px;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        background-color: #f30;
      }
      
      .footer-bg {
        position: relative;
        z-index: 1;
        width: 100%;
        height: 100px;
        float: none;
      }
      
      .snowman {
        position: absolute;
        display: block;
        margin-right: auto;
        margin-left: auto;
      }
      
      .row {
        position: relative;
        width: 960px;
        height: 100px;
        -ms-flex-item-align: start;
        align-self: flex-start;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
      }
      
      .footer-container {
        background-color: transparent;
      }
      
      .pre-footer-container {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        padding-top: 0px;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        background-color: #f30;
      }
      
      .pre-footer-container-div {
        position: relative;
        left: 281px;
        top: 49px;
        right: 0px;
        bottom: 0px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 136px;
        margin-top: 0px;
        margin-right: auto;
        margin-left: auto;
        padding-top: 0px;
        float: right;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -ms-flex-item-align: end;
        align-self: flex-end;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
      }
    }
    @media (min-width: 1280px) {
      ._2nd-container-left-imgdiv {
        left: -150px
      }
    }
    @media (max-width: 1280px) {
      .leftside-img {
        left: 0px;
        top: -175px;
      }
      
      ._2nd-container-left-imgdiv {
        left: -150px
      }
      
      .embed-container iframe {
        position: absolute;
        top: 0;
        left: 25% !important;
        width: 100%;
        height: 100%;
      }
    }
    @media (min-width: 991px) {
      ._2nd-container-left-imgdiv {
        left: -200px
      }
    }
    @media (max-width: 991px) {
      .container-full {
        position: relative;
        z-index: 26;
      }
      
      .embed-container iframe {
        position: absolute;
        top: 0;
        left: 15% !important;
        width: 100%;
        height: 100%;
      }
      
      .container-main-content {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 59%;
        max-width: auto;
        padding-left: 0px;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
      }
      
      .logo {
        display: block;
        margin-left: 0px;
        padding-bottom: 7px;
        padding-left: 0px;
      }
      
      .flags {
        padding-bottom: 11px;
      }
      
      .flags.mobile-flags {
        display: block;
        margin-right: 0px;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
      }
      
      .leftside-img {
        left: 0px;
        top: -175px;
      }
      
      .rightside-img {
        top: -122px;
      }
      
      .middle-img {
        top: -245px;
      }
      
      .middle-text-heading {
        font-size: 25px;
        line-height: 30px;
      }
      
      ._2nd-container-left-imgdiv {
        display: none;
        width: 10%;
        height: auto;
        margin-right: auto;
        margin-left: auto;
        -ms-flex-item-align: center;
        align-self: center;
      }
      
      ._2nd-container-right-imgdiv {
        left: 395px;
        top: -191px;
        -ms-flex-item-align: center;
        align-self: center;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
      }
      
      .footer-bg {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        -webkit-box-align: end;
        -ms-flex-align: end;
        align-items: flex-end;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
      }
      
      .pre-footer-container-div {
        left: 185px;
        top: 33px;
        right: 0px;
      }
    }
    @media (max-width: 767px) {
      ._2nd-container-right-imgdiv {
        display: none;
      }
    }
    @media (max-width: 479px) {
      .container-full {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        clear: left;
      }
      
      .embed-container iframe {
        position: absolute;
        top: 0;
        left: 0 !important;
        width: 100%;
        height: 100%;
      }
      
      .container-main-content {
        width: 45%;
        height: 60px;
        max-width: auto;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
      }
      
      .logo {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        width: 108%;
        margin-left: 0px;
        padding-left: 0px;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -ms-flex-item-align: end;
        align-self: flex-end;
      }
      
      .flags {
        position: static;
        right: 0px;
        bottom: 0px;
        display: block;
        width: 100%;
        max-width: 102px;
        margin-right: 100px;
        padding-left: 0px;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        justify-content: flex-end;
        -ms-flex-item-align: center;
        align-self: center;
        -webkit-box-ordinal-group: 2;
        -ms-flex-order: 1;
        order: 1;
      }
      
      .flags.mobile-flags {
        max-width: auto;
        margin-right: 0px;
        margin-left: 50px;
        padding-top: 8px;
        padding-bottom: 0px;
        -webkit-box-flex: 0;
        -ms-flex: 0 auto;
        flex: 0 auto;
      }
      
      .main-content {
        max-width: auto;
      }
      
      .leftside-img {
        display: none;
      }
      
      .rightside-img {
        display: none;
      }
      
      .middle-img {
        top: -210px;
        display: block;
        padding-top: 98px;
      }
      
      .middle-text {
        padding: 0px 10px;
      }
      
      .text-block {
        line-height: 19px;
      }
      
      .middle-text-heading {
        font-size: 19px;
        line-height: 19px;
      }
      
      ._2nd-section-img-container {
        display: none;
      }
      
      .pre-footer-container-div {
        left: 0px;
        top: 78px;
        right: 0px;
        width: 100%;
      }
      
      .link-media {
        max-width: 25px;
      }
    }
    @font-face {
      font-family: 'Skanskasanspro bold';
      src: url('../fonts/SkanskaSansPro-Bold.otf') format('opentype');
      font-weight: 400;
      font-style: normal;
    }
    @font-face {
      font-family: 'Skanskasanspro regular';
      src: url('../fonts/SkanskaSansPro-Regular.otf') format('opentype');
      font-weight: 400;
      font-style: normal;
    }
    @font-face {
      font-family: 'Skanskasanspro light';
      src: url('../fonts/SkanskaSansPro-Light.otf') format('opentype');
      font-weight: 400;
      font-style: normal;
    }
    .embed-container {
      position: relative;
      height: 400px;
      overflow: hidden;
      max-width: 100%;
      background-color: #f30;
      margin: 0 auto;
    }
    .embed-container iframe,
    .embed-container object,
    .embed-container embed {
      position: absolute;
      top: 0;
      left: 35%;
      width: 100%;
      height: 100%;
    }
    </style>
  </head>

  <body onload="register_popup_close()">
    <div id="email-popup">
      <div id="popupEmailForm">
        <form action="index.php" method="post" id="form">
          <input name="name" type="text" class="feedback-input" placeholder="Name" />
          <input name="to" type="text" class="feedback-input" placeholder="Email" />
          <textarea name="message" class="feedback-input" placeholder="Message"></textarea>
          <input type="submit" value="SUBMIT" />
        </form>
      </div>
    </div>
    <div class="container-full">
      <div class="flags mobile-flags">
        <a class="link-media w-inline-block" href="#"><img src="images/nyelv_ro-teszt.svg" width="40">
        </a>
        <a class="link-media w-inline-block" href="#"><img src="images/nyelv_hu-teszt.svg" width="40">
        </a>
        <a class="link-media w-inline-block" href="#"><img src="images/nyelv_pl-teszt.svg" width="40">
        </a>
        <a class="link-media w-inline-block" href="#"><img src="images/nyelv_en-proba.svg" width="40">
        </a>
      </div>
      <div class="container-main-content">
        <div class="logo"><img class="logo" src="images/skanska_logo-jpg.jpg">
        </div>
      </div>
    </div>
    <div class="first-section" data-ix="rightside">
      <div class="top-cloud"><img class="top-image" src="images/felhok_fent.svg">
      </div>
      <div class="main-content" data-ix="rightside">
        <div class="leftside-img" data-ix="clickpicture">
          <a class="eng-lilghtbox w-inline-block w-lightbox" href="#"><img src="images/gomb_en.svg" width="240">
            <script class="w-json" type="application/json">
              { "items": [{ "type": "image", "_id": "5845d680985d8c620c0009bb", "fileName": "pop_up_2.jpg", "origFileName": "pop_up_2.jpg", "width": 1920, "height": 1080, "fileSize": 243797, "url": "images/pop_up_2.jpg" }, { "type": "image", "_id": "5845e178063be4960d432c28", "fileName": "Képkivágás.PNG", "origFileName": "Képkivágás.PNG", "width": 570, "height": 506, "fileSize": 276989, "url": "images/Képkivágás.PNG" }, { "type": "image", "_id": "5845e181063be4960d432c2d", "fileName": "Specialized Diverge « kerékpár « ORSZÁGÚTI CYCLOCROSS KERÉKPÁR « Kategóriák tallózása « kerékpár apróhirdetés « Bikemag apró.png", "origFileName": "Specialized Diverge « kerékpár « ORSZÁGÚTI CYCLOCROSS KERÉKPÁR « Kategóriák tallózása « kerékpár apróhirdetés « Bikemag apró.png", "width": 1018, "height": 754, "fileSize": 995495, "url": "images/Specialized-Diverge-«-kerékpár-«-ORSZÁGÚTI---CYCLOCROSS-KERÉKPÁR-«-Kategóriák-tallózása-«-kerékpár-apróhirdetés-«-Bikemag-apró.png" }] }
            </script>
          </a>
        </div>
        <div class="middle-img w-clearfix"><img src="images/gomb.svg" width="400">
          <div class="middle-text w-clearfix">
            <h3 class="middle-text-heading">We wish you and your loved ones a wonderful festive season and<br>
prosperous, healthy New Year!</h3>
            <div class="text-block">When the festive season is coming around.
              <br>
              <br> It is time to think about our year and take a look on what we have done.
              <br>
              <br> Besides
              <br> building offices that are inspiring, comfortable and environmentally
              <br> sustainable at the same time, we also find it important to be active in the
              <br> fields of social responsibility
              <br>
              <br>
              <br> Visit our Christmas microsite where you can learn about what kind of activities the countries of the region have done in the name of social responsibility!
            </div>
          </div>
        </div>
        <div class="rightside-img" data-ix="rightside"><img src="images/gomb_pl.svg" width="250">
        </div>
      </div>
    </div>
    <div class="second-section">
      <div class="_2nd-section-img-container" data-ix="rightside">
        <div class="_2nd-container-left-imgdiv"><img height="649" src="images/gomb_hu.svg">
        </div>
        <div class="_2nd-container-right-imgdiv"><img height="467" src="images/gomb_ro.svg" width="140">
        </div>
      </div>
    </div>
    <div class="embed-container">
      <div class="w-embed w-iframe">
        <div class="embed-container">
          <iframe src="iframe/index.html" style="border:0"></iframe>
        </div>
      </div>
    </div>
    <div style="background-color: #f30;">
      <center>
        <button class="popupbutton" id="popup" style="width: 40%;" onclick="popup_show()">Küldjön üdvözletet ismerőseinek!</button>
      </center>
    </div>
    <div class="pre-footer-container w-clearfix">
      <div class="pre-footer-container-div"><img src="images/13FEDF1.png" width="-1">
      </div>
    </div>
    <div class="footer">
      <div class="footer-bg"><img src="images/felhok_lent.svg">
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="js/webflow.js" type="text/javascript"></script>
    <script src="https://cdn.rawgit.com/loktar00/JQuery-Snowfall/master/dist/snowfall.jquery.min.js"></script>

    <script src="js/index.js"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  </body>

  </html>
$("body").snowfall({
    image: "http://www.norbertbarna.com/hopehely_kp.png",
    minSize: 10,
    maxSize: 32
});

$('h1').fitText();

function register_popup_close(e) {
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            popup_hide();
        }
    });

    $("#email-popup").click(function(event) {
        popup_hide();
    });

    $("#popupEmailForm").click(function(event) {
        stopPropagation(event);
    });
}

function stopPropagation(e) {
    e.stopPropagation();
}

//Function To Display Popup
function popup_show() {
    document.getElementById('email-popup').style.display = "block";
}

//Function to Hide Popup
function popup_hide(){
    document.getElementById('email-popup').style.display = "none";
}
